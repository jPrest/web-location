
function render() {
    var list = document.getElementById('list')
    list.innerHTML = ""
    read().forEach(element => {
        var li = document.createElement('li')
        li.innerText = JSON.stringify(element)
        list.appendChild(li)
    });
}

function read() {
    var positions = localStorage.getItem('positions')
    var parsed = JSON.parse(positions)
    if (!parsed) parsed = []
    return parsed;
}

function clearPositions() {
    console.log('clearing')
    localStorage.removeItem('positions')
    render()
}

function write(position) {
    if (!position) return;
    var positions = read()
    positions.push(position);
    const updated = JSON.stringify(makeStringifyable(positions));
    localStorage.setItem('positions', updated);
}

// position object cannot be stringyfied by default
function makeStringifyable(obj) {
    if (obj === null || !(obj instanceof Object)) {
        return obj;
    }
    var temp = (obj instanceof Array) ? [] : {};
    // ReSharper disable once MissingHasOwnPropertyInForeach
    for (var key in obj) {
        temp[key] = makeStringifyable(obj[key]);
    }
    return temp;
}

function getPos() {
    navigator.geolocation.getCurrentPosition(function (position) {
        console.log(position)
        write(position);
        render()
    })
}

var intervalId;
function start() {
    if (intervalId) {
        clearInterval(intervalId)
    }
    getPos();
    var interval = document.getElementById('interval').value * 1000;
    intervalId = setInterval(function () {
        getPos();
    }, interval)
}

function stop() {
    if (intervalId) {
        clearInterval(intervalId);
    }
}

window.onload = function () {
    render()
}